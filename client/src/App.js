
import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'

import Storage from './Storage'

import Login from './screens/login'
import Register from './screens/register-user'
import Home from './screens/home'
import Prices from './screens/prices'
import Payment from './screens/payment'
import FormPost from './screens/formPost'
import PostList from './screens/post-list'
import FormPostEdit from './screens/formPostEdit'

import './App.css'

class App extends Component {

	render() {

		const isActive = true;//Storage.sessionActive()

		return (
			<div>
				{!isActive ? <Authorized /> : <NotAuthorized />}
			</div>
		);
	}
}

export default App


export const Authorized = () => {

	return (
		<Switch>
			{/* <Route path="/" exact={true} component={Home} /> */}
			{/* <Route component={Error} /> */}
		</Switch>
	)
}

export const NotAuthorized = () => {

	return (
		<Switch>
			<Route path="/" exact={true} component={FormPost} />
			<Route exact path="/login" component={Login} />
			<Route exact path="/register" component={Register} />
			<Route exact path="/prices" component={Prices} />
			<Route exact path="/payment" component={Payment} />
			<Route exact path="/postlist" component={PostList} />
			<Route exact path="/formEdit/:id" component={FormPostEdit} />
			<Route component={Error} />
		</Switch>
	)
}

