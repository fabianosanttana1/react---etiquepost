import React, { Component } from 'react';

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';

import './styles.scss'

class HeaderBar extends Component {


    render() {
        return (
            <AppBar position="static" color="default" style={{ position: 'relative' }} >
                <Toolbar style={{ justifyContent: 'space-between' }}>
                    <div>
                        <Typography variant="h6" color="inherit" noWrap >
                            <a onClick={() => this.props.history.push('/')}>EtiquePost</a>
                        </Typography>
                    </div>
                    <div>
                        <Button onClick={() => this.props.history.push('/prices')}>Plano e preços</Button>
                        <Button onClick={() => this.props.history.push('/login')} color="primary" variant="outlined">Login</Button>
                    </div>
                </Toolbar>
            </AppBar>
        );
    }
}


export default withRouter(HeaderBar);