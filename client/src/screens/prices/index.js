import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import StarIcon from '@material-ui/icons/StarBorder';
import Typography from '@material-ui/core/Typography';
import HeaderBar from '../../components/header-bar'

import { textInfo } from './styles'


class Price extends Component {


	state = {
		tiers: [
			{
				title: 'Gratuito',
				price: '',
				description: ['6 etiquetas/dia'],
				buttonText: 'Comece já',
				buttonVariant: 'outlined',
				address: "/register"
			},
			{
				title: 'Premium',
				subheader: 'Mais popular',
				price: 'R$3.000,00',
				description: [
					'150.000 etiquetas',
					'Área exclusiva',
					'Token para implementação'
				],
				buttonText: 'Adquirir',
				buttonVariant: 'contained',
				address: "/payment"
			},
			{
				title: 'Pro',
				price: 'R$1.500,00',
				description: [
					'90.000 etiquetas',
					'Área exclusiva',
					'Token para implementação'
				],
				buttonText: 'Adquirir',
				buttonVariant: 'outlined',
				address: "/payment"
			},
		]
	}


	render() {
		return (
			<React.Fragment>
				<HeaderBar />
				<main style={{ padding: 30 }}>
					{/* Hero unit */}
					<div >
						<Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
							Preços
          </Typography>
						<Typography style={textInfo} variant="h6" align="center" color="textSecondary" component="p">
							Veja nossos planos e escolha o que mais se adequa ao seu negócio, claro que se for preciso mudar de plano
							futuramente isso poderá ser feita de maneira fácil e segura.
          </Typography>
					</div>
					{/* End hero unit */}
					<Grid container spacing={40} alignItems="flex-end">
						{this.state.tiers.map(tier => (
							// Enterprise card is full width at sm breakpoint
							<Grid item key={tier.title} xs={12} sm={tier.title === 'Enterprise' ? 12 : 6} md={4}>
								<Card style={{ marginTop: 50 }}>
									<CardHeader
										title={tier.title}
										subheader={tier.subheader}
										titleTypographyProps={{ align: 'center' }}
										subheaderTypographyProps={{ align: 'center' }}
										action={tier.title === 'Premium' ? <StarIcon /> : null}

									/>
									<CardContent>
										<div >
											<Typography style={{textAlign:'center'}} component="h2" variant="h3" color="textPrimary">
												{tier.price}
											</Typography>
											<Typography variant="h6" color="textSecondary">
											</Typography>
										</div>
										{tier.description.map(line => (
											<Typography variant="subtitle1" align="center" key={line}>
												{line}
											</Typography>
										))}
									</CardContent>
									<CardActions >
										<Button fullWidth variant={tier.buttonVariant} color="primary" onClick={() => this.props.history.push(tier.address)}>
											{tier.buttonText}
										</Button>
									</CardActions>
								</Card>
							</Grid>
						))}
					</Grid>
				</main>
			</React.Fragment>
		);
	}
}


export default Price;