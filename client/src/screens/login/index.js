import React, { Component } from 'react';

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormHelperText from '@material-ui/core/FormHelperText';
import Notifier, { openSnackbar } from '../../components/notifier';


import { logInUser } from '../../actions/authetication'
import './styles.scss'

class Login extends Component {

    state = {
        email: "",
        password: "",
        message: '',
        error: false,
    }




    _onChange = (event) => {
        if (event.target.name == "email") {

            this.setState({ email: event.target.value }, () => {
            })
        } else if (event.target.name == "password") {
            this.setState({ password: event.target.value })
        }
    }

    _logInUser = () => {
        this.props.logInUser(this.state.email, this.state.password)
    }

    _validateForm = () => {
        if (this.state.email == '' || this.state.password == '') {
            return this.setState({ error: true, message: 'Todos os campos devem ser preenchidos' })
        }
        this._logInUser()
    }

    _toRegister = () => {
        this.props.history.push('/register')
    }

    render() {
        return (
            <div className="container-login">
                <Card className="container-login--card-login">
                    <Avatar>
                        <LockIcon />
                    </Avatar>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="email">E-mail</InputLabel>
                        <Input error={this.state.errorEmail} id="email" value={this.state.email} name="email" autoComplete="email" autoFocus onChange={this._onChange} />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Senha</InputLabel>
                        <Input
                            error={this.state.errorPassword}
                            name="password"
                            type="password"
                            id="password"
                            value={this.state.password}
                            autoComplete="current-password"
                            onChange={this._onChange}
                        />
                    </FormControl>

                    {this.state.error && < FormHelperText className="container-login--component-error-text">{this.state.message}</FormHelperText>}
                    {this.props.loading ?
                        <CircularProgress size={40} /> :
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={this._validateForm}>
                            Logar
                    </Button>
                    }

                    <Button onClick={this._toRegister}>Não tem cadastro! Faça o aqui.</Button>
                </Card>
            </div >
        );
    }
}
const mapStateToProps = state => (
    {
        loading: state.AuthenticationReducer.loading,
        errorAuthentication: state.AuthenticationReducer.errorAuthentication,
        sucessAuthetication: state.AuthenticationReducer.sucessAuthetication,
    }
)


export default withRouter(connect(mapStateToProps, { logInUser })(Login));