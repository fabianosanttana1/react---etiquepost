import React, { Component } from 'react';

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import HeaderBar from '../../components/header-bar'


import './styles.scss'

class Home extends Component {


    render() {
        return (
            <div>

                <HeaderBar />
            </div>
        );
    }
}
const mapStateToProps = state => (
    {
    }
)


export default withRouter(connect(mapStateToProps, null)(Home));