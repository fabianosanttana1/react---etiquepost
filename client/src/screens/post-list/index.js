import React, { Component } from 'react';

import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import HeaderBar from '../../components/header-bar'
import AddIcon from '@material-ui/icons/Add';

class PostList extends Component {

  constructor(props) {
    super(props)

    this.state = {
      list: this.props.tickets
    }
  }

  render() {
    return (
      <React.Fragment>
        <HeaderBar />
        <div>
          {
            this.state.list.map((ticket, index) =>
              <Card style={ {margin: '10px'} } >
                <CardContent style={ {width: '42%', display: 'inline-block'} }>
                <Typography color="textSecondary" gutterBottom style={ { "font-weight" : "bold" } }>
                Destinatário
              </Typography>
                  <Typography color="textSecondary" gutterBottom>
                    {ticket.destinatario.destinatario} 
                  </Typography>
                  <Typography variant="h5" component="h3">
                    {ticket.destinatario.logradouroDestinatario}, {ticket.destinatario.telefoneDestinatario}
                  </Typography>
                  <Typography color="textSecondary">
                    {ticket.destinatario.bairroDestinatario}
                  </Typography>
                  <Typography component="p">
                    {ticket.destinatario.cidadeDestinatario}
                  </Typography>
                </CardContent>
                <CardContent style={ {width: '42%', display: 'inline-block'} }>
                <Typography color="textSecondary" gutterBottom gutterBottom style={ { "font-weight" : "bold" } }>
                Remetente
              </Typography>
                  <Typography color="textSecondary" >
                    {ticket.remetente.remetente}
                  </Typography>
                  <Typography variant="h5" component="h3">
                    {ticket.remetente.logradouroRemetente}, {ticket.remetente.telefoneRemetente}
                  </Typography>
                  <Typography color="textSecondary">
                    {ticket.remetente.bairroRemetente}
                  </Typography>
                  <Typography component="p">
                    {ticket.remetente.cidadeRemetente}
                  </Typography>
                </CardContent>
                <CardActions style={ { "justify-content": 'flex-end'} }>
                
                <Button size="small" onClick={(event) => {
                  this.props.history.push('/formEdit/'+ index)
                  }}>Alterar</Button>

                  <Button size="small" onClick={(event) => {
                    delete this.state.list[index];
                    let newArray = this.state.list
                    this.setState({ list: newArray })
                  }}>Remover</Button>
                </CardActions>
              </Card>
            )
          }

            <Button color="secondary" style={ {position : 'fixed' , bottom: '30px', right:'30px' } } variant="fab" onClick={() => this.props.history.push('/')}><AddIcon /></Button>
        </div>
      </React.Fragment>
    );
  }
}


const mapStateToProps = state => (
  {
    tickets: state.TicketsReducer.tickets
  }
)
export default withRouter(connect(mapStateToProps, null)(PostList));