import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Notifier, { openSnackbar } from '../../components/notifier';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import './styles.scss'

class Register extends Component {

    constructor() {
        super()

        this.state = {
            email: "",
            password: "",
            name: ""
        }

        this._onClick = this._onClick.bind(this)
    }

    _onClick = (event) => {
        event.preventDefault();
        fetch('http://localhost:8000/users', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        })
            .then(function (response) {
                return response.json()
            })
            .then(function (body) {
                console.log(body);
                if (body && body.length > 0)
                    body.forEach(element => {
                        console.log(element);
                        openSnackbar({ message: element.msg });
                    });
                else if (body.code) {

                    openSnackbar({ message: body.message });
                }
                else if (body.error && body.error == "false") {

                    openSnackbar({ message: body.message });
                }
            });
    }

    render() {
        return (
            <div className="container-login">
                <Card className="container-login--card-login">
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="email">E-mail</InputLabel>
                        <Input id="email" name="email" autoComplete="email" autoFocus onChange={(event) => this.setState({ email: event.target.value })} />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="name">Nome completo</InputLabel>
                        <Input
                            name="name"
                            type="text"
                            id="name"
                            onChange={(event) => this.setState({ name: event.target.value })}
                        />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Senha</InputLabel>
                        <Input
                            name="password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={(event) => this.setState({ password: event.target.value })}
                        />
                    </FormControl>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        onClick={this._onClick}
                        color="primary">
                        Criar Conta
                    </Button>
                </Card>
                <Notifier />
            </div>
        );
    }
}
const mapStateToProps = state => (
    {
        loading: state.AuthenticationReducer.loading,
    }
)


export default withRouter(connect(mapStateToProps, null)(Register));
