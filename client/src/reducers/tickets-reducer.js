import { TICKETS, ALTER_TICKETS } from "../actions/constants"

const INITIAL_STATE = {
    tickets: []
}

export default (state = INITIAL_STATE, action) => {


    switch (action.type) {

        case TICKETS:
            let payload = state.tickets
            if (state.tickets.length >= 6) {
                state.tickets.pop();
            }
            payload.push(action.payload)
            return { ...state, tickets: payload }
        case ALTER_TICKETS:
            let value = state.tickets
            value[parseInt(action.payload.idTicket)] = action.payload.ticket
            return { ...state, tickets: value }

        default:
            return state;
    }
}