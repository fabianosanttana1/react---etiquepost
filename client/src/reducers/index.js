import { combineReducers } from 'redux';
import AuthenticationReducer from './authentication-reducer';
import TicketsReducer from './tickets-reducer';

export default combineReducers({
    AuthenticationReducer: AuthenticationReducer,
    TicketsReducer: TicketsReducer
});