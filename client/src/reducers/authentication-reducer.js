import { ERROR_AUTHENTICATION, WAIT_AUTHENTICATION, SUCESS_AUTHENTICATION } from "../actions/constants"

const INITIAL_STATE = {
    userInfo: {},
    loading: false,
    sucessAuthentication: {},
    errorAuthentication: {}
}

export default (state = INITIAL_STATE, action) => {


    switch (action.type) {

        case SUCESS_AUTHENTICATION:
            return { ...state, sucessAuthentication: action.payload, loading: false }

        case ERROR_AUTHENTICATION:
            return { ...state, errorAuthentication: action.payload, loading: false }

        case WAIT_AUTHENTICATION:
            return { ...state, loading: action.payload }

        default:
            return state;
    }
}