const consume = (key, data, json = false) => {
    try {
        if (data === undefined) {
            const val = localStorage.getItem(key)

            if (val && json) {
                return JSON.parse(val) || {}
            } else {
                return json ? {} : val
            }
        } else {
            if (json) {
                return localStorage.setItem(key, JSON.stringify(data))
            } else {
                return localStorage.setItem(key, data)
            }
        }
    } catch (e) {
        return json ? {} : null
    }
}

/**
 * Responsável pelo consumo dos dados guardados em storage;
 *
 * GET: Quando chamado sem parametros irá retornar os dados salvos para chave específica;
 * SET: Quando chamado com parâmetros irá adicionar ou atualizar os dados;
 */
export default class Storage {

    // @object
    static oauth = (data) => {
        return consume('@ETIQUEPOST:oauth', data, true)
    }

    // @object
    static user = (data) => {
        return consume('@ETIQUEPOST:user', data, true)
    }

    // @boolean
    static sessionActive = (data) => {
        return consume('@ETIQUEPOST:sessionActive', data)
    }

    static clear = () => {
        localStorage.clear()
    }
}