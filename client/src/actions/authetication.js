import { ERROR_AUTHENTICATION, WAIT_AUTHENTICATION, SUCESS_AUTHENTICATION, WAIT_REGISTER } from "./constants"
import { logInUserService, registerUserService } from "../services/authentication-service"
import Storage from '../Storage'

export const logInUser = (email, password) => {
    return async dispatch => {
        dispatch({ type: WAIT_AUTHENTICATION, payload: true });
        try {
            let response = await logInUserService({ email, password });

            if (response.error) {
                dispatch({ type: ERROR_AUTHENTICATION, payload: response })
                alert(response.mensagem)
            } else {
                Storage.sessionActive(true)
                Storage.oauth(response.accessToken)
                Storage.user(response)
                dispatch({ type: WAIT_AUTHENTICATION, payload: false });
                dispatch({ type: SUCESS_AUTHENTICATION, payload: "Login Realizado com sucesso." })
                alert("Login Realizado com sucesso")
            }
        } catch (err) {
            dispatch({
                type: ERROR_AUTHENTICATION,
                payload: err
            })
        }
    }
}

export const registerUser = (data) => {
    return async dispatch => {
        dispatch({ type: WAIT_REGISTER, payload: true });
        try {
            let response = await registerUserService(data);

            if (response.error) {
                dispatch({ type: ERROR_AUTHENTICATION, payload: response })
            } else {

            }
        } catch (err) {
            dispatch({
                type: ERROR_AUTHENTICATION,
                payload: err
            })
        }
    }
}