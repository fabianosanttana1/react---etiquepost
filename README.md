![](https://media.giphy.com/media/WExZGIqQYk5MI/source.gif)

> Etiquepost é um sistema desenvolvido para a disciplina de Análise de Sistemas com o intuito de aguçar a busca por novos conhecimentos no aluno. Desenvolvi esse sistema no início do ano passado para o trabalho disciplinar.

## Tecnologias utilizadas

* [ReactJS](https://reactjs.org/)
* [JavaScript/es6](http://github.com/DrkSephy/es6-cheatsheet)
* [SASS](http://github.com/sass/sass)
